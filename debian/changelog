libanyevent-rabbitmq-perl (1.22~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Debian Janitor ]
  * use secure copyright file specification URI
  * build-depend on debhelper-compat (not debhelper)
  * set upstream metadata fields:
    Repository Repository-Browse

  [ Jonas Smedegaard ]
  * use debhelper compatibility level 13 (not 9)
  * declare compliance with Debian Policy 4.6.0
  * stop build-depend explicitly on perl:
    not called directly during build
  * update watch file: use dversionmangle=auto
  * update copyright info:
    + use Reference field (not License-Reference);
      update lintian overrides
    + use secure URI for Contact-Info
    + update coverage
    + stop track no longer included files
  * simplify source helper script copyright-check
  * build-depend on libfile-sharedir-install-perl

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 12 Sep 2021 11:36:05 +0200

libanyevent-rabbitmq-perl (1.19+dfsg-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules
    Stop build-depend on licensecheck cdbs.
  * Update watch file:
    + Rewrite usage comment.
    + Generalize dversionmangle regular expression.
    + Use substitution strings.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Set Rules-Requires-Root: no.
  * Relax to (build-)depend unversioned on amqp-specs.
  * Mark build-dependencies needed only for testsuite as such.
  * Wrap and sort control file.
  * Explain AnyEvent in long description.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Update copyright info: Extend coverage of packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 18 Feb 2019 21:57:18 +0100

libanyevent-rabbitmq-perl (1.19+dfsg-1) unstable; urgency=medium

  [ upstream ]
  * (no changes: Re-release properly renamed to indicate repackaging)

  [ Jonas Smedegaard ]
  * Modernize Vcs-Git field: Use https URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Update copyright info:
    + Extend coverage of packaging to include current year.
  * Stop track upstream source with CDBS (use gpb --uscan).
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to version 4.
    + Use only MetaCPAN URL.
    + Tighten to ignore prereleases.
    + Mention gpb in usage comment.
  * Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Sep 2016 18:48:54 +0200

libanyevent-rabbitmq-perl (1.19-2) unstable; urgency=medium

  * Team upload

  * mark package as autopkg-testable

 -- Damyan Ivanov <dmn@debian.org>  Sun, 22 Nov 2015 11:24:26 +0000

libanyevent-rabbitmq-perl (1.19-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Add bind_exchange and unbind_exchange methods for
      exchange-exchange bindings.
    + Add no_ack as an optional argument to ->consume method.
    + Improve documentation.

  [ Lucas Kanashiro ]
  * Declare compliance with Debian Policy 3.9.6.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of packaging to include current year.
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Fix lintian overrides.
  * Override lintian regarding build-depending unversioned on debhelper.
  * Modernize git-buildpackage config: Avoid git- prefix.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 04 Aug 2015 09:59:36 +0200

libanyevent-rabbitmq-perl (1.17~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Add support for chunking large bodies into multiple AMQP frames,
      allowing the sending of large messages.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 26 Aug 2014 23:02:23 +0200

libanyevent-rabbitmq-perl (1.16~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Doc fixes.
    + Fix leak when calling ->close + tests.

  [ Jonas Smedegaard ]
  * Fix only depend on (not also recommend) amqp-specs.
  * Update copyright info:
    + Extend coverage of packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 May 2014 09:45:06 +0200

libanyevent-rabbitmq-perl (1.15~dfsg-2) unstable; urgency=medium

  * Team upload

  * fix symlink to fixed_amqp0-9-1.xml
    Closes: #735532 -- Invalid location of fixed_amqp0-9-1.xm
  * add amqp-specs as a direct dependency
  * Declare conformance with Policy 3.9.5
  * migrate search.cpan.org URLs to metacpan.org
  * use cannonical packaging Git URL

 -- Damyan Ivanov <dmn@debian.org>  Thu, 16 Jan 2014 13:01:16 +0200

libanyevent-rabbitmq-perl (1.15~dfsg-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#703660.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 21 Jul 2013 18:29:13 +0200
